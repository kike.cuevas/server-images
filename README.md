# Building QEMU-KVM Linux Server Images with Packer

## Introduction

This project helps you build server images with the server templating tool Packer from Hashicorp,
Packer is a tool for creating machine and container images for multiple platforms from a single
source configuration.

The follow is a list of main features:

 * It is easy to use and automates the creation of any type of machine image.
 * Embraces modern configuration management to install and configure software within images.
 * Multiple virtualization providers: Virtualbox, QEMU, Amazon EC2, Docker and more.

We are going to use packer to build machine images for DevOps and Wordpress services, we will use
the QEMU provider to create kvm-qemu machines for local environments. These machine images will
be based on Ubuntu 16.04 on 64-bit (Xenial).

## Objetives

The follow is a list of main objetives:

 * Build server images for Ubuntu 16.04 on 64-bit for QEMU.
 * Provision server images with shell scripts and configuration management tools.
 * Publish the server image template artifact on shared repository.

## Requirements

In order to programatically build a ubuntu-16.04 64-bit server image from the ground, you will need a physical machine with the follow:

* Linux (ubunut-16.04 recommended)
* Packer 1.2.x
* QEMU 2.5
* ISO media
* Preseed file
* Packer jason file
* Provision scripts

We don't cover the setup instructions for this components.

## Structure

We need to be sure we have a directory structure like this inside the server-images directory:

```shell
[sysadmin@linux-kvm-build][~/vcs/koolops/server-images]
$ tree -d
.
├── bin
├── builds
│   └── libvirt
├── http
└── iso

5 directories
```

Now we check inside the http directory, the preseed.cfg file is located.

```shell
[sysadmin@linux-kvm-build][~/vcs/koolops/server-images]
$ ls -l http/
total 4
-rw-rw-r-- 1 sysadmin sysadmin 1676 Nov 27 11:49 preseed.cfg
```

We store the installation media in the iso directory:

```shell
[sysadmin@linux-kvm-build][~/vcs/koolops/server-images]
$ ls -l iso/
total 592900
-rw-rw-r-- 1 sysadmin sysadmin 869269504 Nov 27 13:07 ubuntu-16.04.2-server-amd64.iso
```

If you store your iso files on another location, you can symlink, for example:

$ ls -l iso/
total 2
lrwxrwxrwx 1 sysadmin sysadmin 54 Oct  4 14:01 ubuntu-16.04.2-server-amd64.iso -> /repositorio/iso/redhat/ubuntu-16.04.2-server-amd64.iso

IMPORTANT: If you haven't had downloaded the iso, don't worry, packer will download it for you.

IMPORTANT: Be sure the ISO's sha256 checksum matches the one on the .json file.

## Usage

First, we need to be sure we are located at the server-images directory:

```shell
[sysadmin@linux-kvm-build][~]
$ cd ~/vcs/koolops/server-images
```

We validate the template:

```shell
[sysadmin@linux-kvm-build][~/vcs/koolops/server-images]
$ packer validate ubuntu-16.04.json
Template validated successfully.
```

And now build the image:

```shell
[sysadmin@linux-kvm-build][~/vcs/koolops/server-images]
$ packer build ubuntu-16.04.json
```

The output will be something like the follow:

```shell
[sysadmin@linux-kvm-build][~/vcs/koolops/server-images]

```

IMPORTANT: We use the --force option to override any previous .qcow2 image.

The result is the following qcow2 image for libvirt in the builds directory:

```shell
[sysadmin@linux-kvm-build][~/vcs/koolops/server-images]
$ ls -lh builds/
total 598M
-rw-rw-r-- 1 sysadmin sysadmin    0 Nov 28 13:02 empty
```

## References

 * Packer: https://www.packer.io
 * Vagrant: https://www.vagrantup.com
 * Ansible: https://www.ansible.com
 * Build and Image: https://www.packer.io/intro/getting-started/build-image.html
 * https://github.com/sotayamashita/packer-example/blob/master/packer/vagrant/base.json
 * https://github.com/p0bailey/packer-templates
 * https://github.com/p0bailey/ansible-packer/blob/master/tasks/main.yml
