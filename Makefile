#
# Makefile for Server Image Builder
#
# author: jorge.medina@koolops.com.mx
# desc: Script to build the server images using packer for the local development environment.

NAME = server-images
VERSION = 1.0.6

IMAGE_NAME_UBUNTU = template-ubuntu-16.04-x64-server.qcow2
IMAGE_NAME_UBUNTU_QCOW = template-ubuntu-16.04-x64-server.qcow
IMAGE_NAME_CENTOS = template-centos7-x64-server.qcow2
IMAGE_NAME_CENTOS_QCOW = template-centos7-x64-server.qcow
IMAGES_REPOSITORY=/repositorio/staging/server-images

.PHONY: all validate build-ubuntu build-centos publish-ubuntu publish-centos clean help

all: help

validate:
	@echo ""
	@echo "Validating ubuntu and centos server templates for qemu-kvm/libvirt."
	@echo ""
	packer validate ubuntu-16.04.json
	packer validate centos7.json

build-ubuntu:
	@echo ""
	@echo "Building ubuntu server image for qemu-kvm/libvirt."
	@echo ""
	packer build ubuntu-16.04.json

build-centos:
	@echo ""
	@echo "Building centos server image for qemu-kvm/libvirt."
	@echo ""
	packer build centos7.json

publish-ubuntu:
	@echo ""
	@echo "Publishing ubuntu server image to network repository."
	@echo ""
	qemu-img convert -f qcow2 -O qcow2 -o compat=0.10 builds/libvirt/${IMAGE_NAME_UBUNTU}/${VERSION}/${IMAGE_NAME_UBUNTU} ${IMAGES_REPOSITORY}/${IMAGE_NAME_UBUNTU_QCOW}


publish-centos:
	@echo ""
	@echo "Publishing centos server image to network repository."
	@echo ""
	qemu-img convert -f qcow2 -O qcow2 -o compat=0.10 builds/libvirt/${IMAGE_NAME_CENTOS}/${VERSION}/${IMAGE_NAME_CENTOS} ${IMAGES_REPOSITORY}/${IMAGE_NAME_CENTOS_QCOW}

clean:
	@echo ""
	@echo "Cleaning local development environment."
	@echo ""
	rm -rf builds/*/*.box
	rm -rf builds/libvirt/*
	rm -rf packer_cache

help:
	@echo ""
	@echo "Please use \`make <target>' where <target> is one of"
	@echo ""
	@echo "  validate		Validates the config file."
	@echo "  build-ubuntu     	Builds the ubuntu server image."
	@echo "  build-centos		Builds the centos server image."
	@echo "  publish-ubuntu		Publishes ubuntu server image to network repository."
	@echo "  publish-centos		Publishes centos server image to network repository."
	@echo "  clean			Cleans local changes and temporary filess."
	@echo ""
	@echo ""
